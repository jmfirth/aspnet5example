﻿/// <binding />
/* Gulpfile Configuration */

// File names
var jsOutputFilename = "combined.js";

// Glob patterns
var sourceJsGlobPattern = "app/**/*.js",
    //sourceJsGlobPatternBase = "app/",
    destJsGlobPattern = "wwwroot/app",
    sourceLessGlobPattern = "less/**/*.less",
    sourceLessFilePath = "less/site.less",
    destCssGlobPattern = "wwwroot/style",
    sourceTemplateGlobPattern = "app/**/*.html",
    //sourceTemplateGlobPatternBase = "app/",
    destTemplateGlobPattern = "wwwroot/app",
    srcIndexTemplatePath = "./app/index.html"
destIndexPath = "./wwwroot/index.html";

/* Gulpfile Dependencies */

// Generic dependencies
var parsePath = require('parse-filepath'),
    gulp = require('gulp'),
    argv = require('yargs').argv,
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    clean = require('gulp-clean'),
    plugins = require('gulp-load-plugins')(),
    bowerFiles = require('main-bower-files'),
    angularFileSort = require('gulp-angular-filesort');

// Javascript dependencies
var jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify');

// CSS dependencies
var less = require('gulp-less'),
    LessPluginCleanCSS = require('less-plugin-clean-css'),
    LessPluginAutoPrefix = require('less-plugin-autoprefix'),
    cleancss = new LessPluginCleanCSS({ advanced: true }),
    autoprefix = new LessPluginAutoPrefix({ browsers: ["last 2 versions"] });

// Angular Template dependencies
var templateCache = require("gulp-angular-templatecache");


/* Entrypoint Tasks */

// Command: gulp
gulp.task("default", ["watch"]);

gulp.task("clean", ["styles:clean", "scripts:clean", "templates:clean", "bower:clean", "inject:clean"]);

// Command: gulp watch
gulp.task("watch", function () {
    gulp.watch(sourceLessGlobPattern, ["styles:debug"]);
    gulp.watch(sourceJsGlobPattern, ["scripts:debug"]);
    gulp.watch(sourceTemplateGlobPattern, ["templates:debug"]);
    gulp.watch(sourceTemplateGlobPattern, ["bower:debug"]);
});

// Command: gulp debug
gulp.task("debug", ["build:debug"]); // ["styles:debug", "scripts:debug", "templates:debug", "bower:debug"]);
gulp.task("Debug", ["debug"]);

// Command: gulp release
gulp.task("release", ["build:release"]); // ["styles:release", "scripts:release", "templates:release", "bower:release"]);
gulp.task("Release", ["release"]);


/* Build Tasks */

gulp.task("build:debug", ["inject:clean", "styles:debug", "scripts:debug", "templates:debug", "bower:debug"], function () {
    return gulp.tasks["inject:common"].fn();
});

gulp.task("build:release", ["inject:clean", "styles:debug", "scripts:debug", "templates:debug", "bower:release"], function () {
    return gulp.tasks["inject:common"].fn();
});


/* Inject Tasks */

gulp.task("inject:clean", function () {
    gulp
        .src("./wwwroot/index.html")
        .pipe(clean());
});

gulp.task("inject:common", ["inject:clean"], function () {
    return gulp
        // using the source index template
        .src(srcIndexTemplatePath)
        // inject bower css
        .pipe(plugins.inject(gulp.src("wwwroot/lib/**/*.css"), {
            name: 'bower',
            addRootSlash: false,
            transform: function (filePath, file, i, length) {
                return '<link rel="stylesheet" href="' + filePath.replace("wwwroot/", "") + '"/>';
            }
        }))
        // inject bower js
        .pipe(plugins.inject(gulp.src("wwwroot/lib/**/*.js").pipe(angularFileSort()), {
            name: 'bower',
            addRootSlash: false,
            transform: function (filePath, file, i, length) {
                return '<script src="' + filePath.replace("wwwroot/", "") + '"></script>';
            }
        }))
        // inject app css
        .pipe(plugins.inject(
          gulp.src([destCssGlobPattern + "/**/*.css"], { read: false }), {
              addRootSlash: false,
              transform: function (filePath, file, i, length) {
                  return '<link rel="stylesheet" href="' + filePath.replace("wwwroot/", "") + '"/>';
              }
        }))
        // inject app js
        .pipe(plugins.inject(
          gulp.src([destJsGlobPattern + "/**/*.js"], { read: false }), {
              addRootSlash: false,
              transform: function (filePath, file, i, length) {
                  return '<script src="' + filePath.replace("wwwroot/", "") + '"></script>';
              }
        }))
        // move to destination path
        .pipe(gulp.dest(parsePath(destIndexPath).dirname));
});


/* Style Tasks */

// Cleans the destination folder containing preprocessed less
gulp.task("styles:clean", function () {
    return gulp
        .src(destCssGlobPattern + "/*")
        .pipe(clean());
});

// Process the less but leave unminified for debugging
gulp.task("styles:debug", ["styles:clean"], function () {
    return gulp
        .src([sourceLessFilePath])
        .pipe(less({
            plugins: [autoprefix]
        }))
        .pipe(gulp.dest(destCssGlobPattern));
});

// Process the less and minify for release
gulp.task("styles:release", ["styles:clean"], function () {
    return gulp
        .src([sourceLessFilePath])
        .pipe(less({
            plugins: [autoprefix, cleancss]
        }))
        .pipe(gulp.dest(destCssGlobPattern));
});


/* Scripts Tasks */

// Clean the destination folder containing scripts
gulp.task("scripts:clean", function () {
    return gulp
        .src(destJsGlobPattern + "/*")
        .pipe(clean());
});

// Copy the transpiled scripts into the destination folder, but leave as-is for debugging
gulp.task("scripts:debug", ["scripts:clean"], function () {
    return gulp
        .src([sourceJsGlobPattern], { base: parsePath(sourceJsGlobPattern).dirname.replace('**', '') }) // { base: sourceJsGlobPatternBase })
        .pipe(jshint())
        .pipe(gulp.dest(destJsGlobPattern));
});

// Copy the transpiled scripts into the destination folder, ready for release
gulp.task("scripts:release", ["scripts:clean"], function () {
    return gulp
        .src([sourceJsGlobPattern], { base: parsePath(sourceJsGlobPattern).dirname.replace('**', '') }) // { base: sourceJsGlobPatternBase })
        .pipe(concat(jsOutputFilename))
        .pipe(jshint())
        .pipe(uglify())
        .pipe(rename({
            extname: '.min.js'
        }))
        .pipe(gulp.dest(destJsGlobPattern));
});


/* Angular Templates */

// Clean the destination folder containing templates
gulp.task("templates:clean", function () {
    return gulp
        .src(destTemplateGlobPattern + "/*")
        .pipe(clean());
});

// Move the templates over in the same folder structure for debug
gulp.task('templates:debug', ["templates:clean"], function () {
    gulp
        .src([sourceTemplateGlobPattern], { base: parsePath(sourceTemplateGlobPattern).dirname.replace('**', '') }) //sourceTemplateGlobPatternBase })
        .pipe(gulp.dest(destTemplateGlobPattern));
});

// Bundle templates into $templateCache with an IIFE and copy to project for release
gulp.task('templates:release', ["templates:clean"], function () {
    gulp
        .src([sourceTemplateGlobPattern], { base: parsePath(sourceTemplateGlobPattern).dirname.replace('**', '') }) //sourceTemplateGlobPatternBase })
        .pipe(templateCache("templates.js", { module: "app", moduleSystem: "IIFE" }))
        .pipe(gulp.dest(destTemplateGlobPattern));
});


/* bower tasks */

// Clean the bower release folder
gulp.task("bower:clean", function () {
    return gulp
        .src('wwwroot/lib/**/*')
        .pipe(clean());
});

// Copy the required bower assets to the destination folder individiually
gulp.task("bower:debug", ["bower:clean"], function () {
    gulp
        .src(bowerFiles("**/*.js"))
        .pipe(gulp.dest('wwwroot/lib/js'));

    return gulp
        .src(bowerFiles("**/*.css"))
        .pipe(gulp.dest('wwwroot/lib/css'));
});

// Bundle the bower assets to the destination folder 
gulp.task('bower:release', ["bower:clean"], function () {
    gulp
        .src(bowerFiles("**/*.js"))
        .pipe(concat("combined.js"))
        .pipe(uglify())
        .pipe(gulp.dest('wwwroot/lib/js'));

    gulp
        .src(bowerFiles("**/*.css"))
        .pipe(concat("combined.css"))
        .pipe(gulp.dest('wwwroot/lib/css'));
});