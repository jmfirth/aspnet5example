# AspNet5Example.EmptyWeb

This is an example of an ASP.NET 5 Empty Website project template.  It uses:

- `npm` for `node.js` dependencies
- `Bower` for web depedencies
- `gulp.js` for task running
- `Less` for style preprocessing

## `Gulp.js` Tasks

The Gulpfile contains many tasks, but only 4 are intended to be run:

```bash
# Clean the assembled application
gulp clean

# Watch the application source, process and shuffle assets as required.  
# Intended to be used either at the CLI or via the Visual Studio 2015 Task Runner
gulp watch

# Assemble the application for debugging
gulp debug
# (an alias)
gulp Debug

# Assemble the application for release
gulp release
# (an alias)
gulp Release
```

The aliases are intended to be used in conjunction with building the project through Visual Studio.  The aliases align with the current release configurations (Debug, Release).  When building in any of these configurations, the appropriate gulp task will be run.  The following XML was added to the bottom of the project file to support this method.

```xml
  <PropertyGroup>
    <CompileDependsOn>
    $(CompileDependsOn);
    GulpBuild;
  </CompileDependsOn>
    <CleanDependsOn>
    $(CleanDependsOn);
    GulpClean
  </CleanDependsOn>
  </PropertyGroup>
  <Target Name="GulpBuild">
    <Exec Command="npm install" />
    <Exec Command="bower install" />
    <Exec Command="gulp $(ConfigurationName)" />
  </Target>
  <Target Name="GulpClean">
    <Exec Command="gulp clean" />
  </Target>
```

## Restoring `NuGet` Packages With `dnu`

`dnu` provides a method of restoring the packages defined within the project.json configuration file.

```bash
# In the web project root folder
dnu restore
```

Note: this step is *only* necessary if you are running the application locally and not using `Visual Studio 2015`.

## Local Execution

There are 3 main options for running the project locally:

- via `IIS Express` in `Visual Studio 2015`
- using the command line `dnx` tool
- using any existing or instanced web server.

Example of using the `dnx` tool:

```bash
# In the web project root folder
dnx . web
```

Example of using an instanced web server (example: `node http-server`)

```bash
# In the web project root folder
cd wwwroot
http-server
```

## `Docker` Release Management

### Assembling the Application

First, run the appropriate `gulp.js` task to assemble the application.

```bash
# In the web project root folder
gulp release
```

### Building the Container

The container description is contained in the web project root.  This Dockerfile leverages Microsoft's latest `ASP.NET vNext docker` available in the `docker` registry at:

> https://registry.hub.docker.com/u/microsoft/aspnet/

```bash
# In the web project root folder
docker build -t aspnet5example-emptyweb .
```

### Running the Container

You should run the container to validate it before going further.

```bash
docker run -d -p 5004:5004 -t aspnet5example-emptyweb
# The web site is now exposed on the docker host at port 5004
```

### Deploying the Container

Once your container is ready it can be saved to a new image and tagged with a version.

```bash
# Find the container id for the container you want to release
docker ps -a | grep aspnet5example-emptyweb
# Using the container id, commit it to a new image
docker commit <container_id> aspnet5example-emptyweb:1.0.0
```

An image snapshot can be saved once the container has been built.

```bash
docker save aspnet5example-emptyweb:1.0.0 > aspnet5example-emptyweb-1.0.0.image.tgz
```

On the docker host, the image can be imported from the tarball image.

```bash
docker load < aspnet5example-emptyweb-1.0.0.image.tgz
```

Once imported, the image can be ran as usual.

```bash
docker run -d -p 5004:5004 -t aspnet5example-emptyweb:1.0.0
```
