# AspNet5Example

This solution contains a sample project for development using ASP.NET 5 empty web sites and release management via the official `Microsoft ASP.NET vNext docker`.

[AspNet5Example.EmptyWeb README](./src/AspNet5Example.EmptyWeb/README.md)
